using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;


public class Main : MonoBehaviour
{
    // public AssetReference spherePrefabRef;

    public RawImage img;
    private void Start()
    {
        // Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/Cube.prefab").Completed += (handle) =>
        // {
        //     //预设物体
        //     GameObject prefabObj = handle.Result;
        //     //实例化
        //     GameObject cubeObj = Instantiate(prefabObj);
        // };
        
        // spherePrefabRef.InstantiateAsync().Completed += (obj) =>
        // {
        //     GameObject sphereObj = obj.Result;
        // };
        Addressables.LoadAssetAsync<Texture2D>("Assets/Textures/wallhaven-o38w67.jpg").Completed += (obj) =>
        {
            //图片
            Texture2D tex2D = obj.Result;
            img.texture = tex2D;
            img.GetComponent<RectTransform>().sizeDelta = new Vector2(tex2D.width*0.1f, tex2D.height*0.1f);
        };

    }


    private void Update()
    {
        
    }
}
